# dp8

Drupal 8 base project.

### Included:
- french translations.
- sql database (dp8.sql).
- Apache virtual host (dp8.test).

### Pre-required:
- Apache.
- Mysql/MariaDB.
- PHP 5.5+.

### Recommended:
- Activate PHP 'opCache' extension.

### How-To:
1) Create a new SQL database named 'dp8' and import the 'dp8.sql' file located in the 'database/' folder in the root directory of the project.
2) Create an Apache virtual host like this:
```CONF
<VirtualHost *:80>
    DocumentRoot "/var/www/html/dp8/" # Where 'DocumentRoot' is the root directory of your project.
    ServerName dp8.test
    ServerAlias *.dp8.test
    <Directory "/var/www/html/dp8/"> # Where 'Directory' is the root directory of your project.
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>

```

3) Edit the file 'settings.php' located in the 'sites/default/' folder and change the user informations to connect to your database.

```php
<?php
$databases['default']['default'] = array (
  'database' => 'dp8',
  'username' => '<dbUser>', //default: root
  'password' => '<dbPass>', // default: no password
  'prefix' => '',
  'host' => 'localhost',
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);
```

### Credentials for admin panel:
- user: admin
- pass: admin

### Optional:
Drupal core can use the Symfony trusted host mechanism to prevent HTTP Host header spoofing.

To enable the trusted host mechanism, you enable your allowable hosts in $settings['trusted_host_patterns'] in the file 'settings.php' located in the 'sites/default/' folder. This should be an array of regular expression patterns, without delimiters, representing the hosts you would like to allow.

For example:
```php
$settings['trusted_host_patterns'] = array(
  '^www\.example\.com$',
);
```
will allow the site to only run from www.example.com.

If you are running multisite, or if you are running your site from different domain names (eg, you don't redirect http://www.example.com to http://example.com), you should specify all of the host patterns that are allowed by your site.

For example:
```php
$settings['trusted_host_patterns'] = array(
  '^example\.com$',
  '^.+\.example\.com$',
  '^example\.org$',
  '^.+\.example\.org$',
);
```
will allow the site to run off of all variants of example.com and example.org, with all subdomains included.

For this project if you want to add your VirtualHost as a trusted host, you can add this code:
```php
$settings['trusted_host_patterns'] = array(
  '^dp8\.test$',
);
```
